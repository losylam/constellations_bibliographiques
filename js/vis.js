// jQuery.noConflict();
var myNetwork, Network, RadialPlacement, width_, height;
var col = ['#ff7f0e','#1f77b4', '#aec7e8' , '#2ca02c', '#ffbb78'];

var em_min = 200;
var em_max = 260;
var em_n = 261;

const dictionnary = {
    "Émissions": {fr: "Émissions", en: "Shows"},
    "Émission": {fr: "Émission", en: "Show"},
    "Auteurs (écrivains, philosophes, poêtes...)": {fr: "Auteurs (écrivains, philosophes, poêtes...)", en: "Writers (poets, philosopher...)"},
    "Scientifiques (chercheurs, médecins...)": {fr: "Scientifiques (chercheurs, médecins...)", en: "Scientists (academics, doctors...)"},
    "Articles scientifiques": {fr: "Articles scientifiques", en: "Academic papers"},
    "du ": {fr: "du ", en: "from "},
    " au ": {fr: " au ", en: " to "},
    "numéros ": {fr: "numéros ", en: "numbers from "},
    " à ": {fr: " à ", en: " to "},
    "Infos Auteur": {fr: "Infos Auteur", en: "Informations about authors"},
    "Infos Émission": {fr: "Infos Émission", en: "Informations about the show"},
    "Infos Article": {fr: "Infos Article", en: "Informations about the paper"},
    "naissance :": {fr: "naissance :", en: "birth:"},
    "décès :": {fr: "décès :", en: "death:"},
    "lien France Inter": {fr: "lien France Inter", en: "France Inter website"},
    "lien Wikipedia": {fr: "lien Wikipedia", en: "Wikipedia link"},
    "numéro :": {fr: "numéro :", en: "number:"},
    "date de diffusion :": {fr: "date de diffusion :", en: "date:"},
    "Auteurs cités" : {fr: "Auteurs cités", en: "Mentioned authors"},
    "Publication :": {fr: "Publication :", en: "Publication:"},
    "Auteurs :": {fr: "Auteurs :", en: "Authors:"},
    "Revue :": {fr: "Revue :", en: "Journal:"},
    "Références": {fr: "Références", en: "References"},
    "Référence": {fr: "Référence", en: "Reference"}
};

let lang = "fr";
if ( sledd_lang ){
    lang = sledd_lang;
}


function _(key){
    if (dictionnary[key]){
        return dictionnary[key][lang];
    }
    return key;
}

// Groups parameters
var dicFilter = {};
dicFilter['emission'] = {'active':true, 'group':0, 'color':'#ff7f0e'};
dicFilter['scientifique'] = {'active':false, 'group':1, 'color':'#1f77b4'};
dicFilter['auteur'] = {'active':true, 'group':2, 'color':'#aec7e8'};
dicFilter['article'] = {'active':false, 'group':3, 'color':'#2ca02c'};
var lstFilter = [dicFilter['emission'],dicFilter['scientifique'],dicFilter['auteur'],dicFilter['article']];

/** useful functions */
var map_range = function(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
};

/** create a legend in the control panel */
var legend = function() {
    var leg;
    leg = d3.select('#legende_emission').append('svg')
	      .attr('height', 14)
	      .attr('width', 100)
	      .append('circle')
	      .attr('r', 7)
	      .attr('cx', 7)
	      .attr('cy', 7)
	      .style('fill', '#ff7f0e');
    d3.select('#legende_emission').selectAll('svg')
	      .append('text')
	      .text(_('Émissions'))
	      .style('fill', '#fff')
	      .attr('x', 25)
	      .attr('y', 13);

    leg = d3.select('#legende_auteur').append('svg')
	      .attr('height', 15)
	      .attr('width', 300)
	      .append('circle')
	      .attr('r', 7)
	      .attr('cx', 7)
	      .attr('cy', 7)
	      .style('fill', '#aec7e8');
    d3.select('#legende_auteur').selectAll('svg')
	      .append('text')
	      .text(_('Auteurs (écrivains, philosophes, poêtes...)'))
	      .style('fill', '#fff')
	      .attr('x', 25)
	      .attr('y', 13);

    leg = d3.select('#legende_scientifique').append('svg')
	      .attr('height', 15)
	      .attr('width', 300)
	      .append('circle')
	      .attr('r', 7)
	      .attr('cx', 7)
	      .attr('cy', 7)
	      .style('fill', '#1f77b4');
    d3.select('#legende_scientifique').selectAll('svg')
	      .append('text')
	      .text(_('Scientifiques (chercheurs, médecins...)'))
	      .style('fill', '#fff')
	      .attr('x', 25)
	      .attr('y', 13);

    leg = d3.select('#legende_article').append('svg')
	      .attr('height', 15)
	      .attr('width', 300)
	      .append('circle')
	      .attr('r', 7)
	      .attr('cx', 7)
	      .attr('cy', 7)
	      .style('fill', '#2ca02c');
    d3.select('#legende_article').selectAll('svg')
	      .append('text')
	      .text(_('Articles scientifiques'))
	      .style('fill', '#fff')
	      .attr('x', 25)
	      .attr('y', 13);
};

/**
 * Object that hancle circular placement
 */
RadialPlacement = function() {
    var values = d3.map();
    var increment = 20;
    var radius = 100;
    var center = {
	      'x': 0,
	      'y': 0
    };
    var start = -90;
    var current = start;

    var radialLocation = function(center, angle, radius) {
	      var x, y;
	      x = center.x + radius * Math.cos(angle * Math.PI / 180);
	      y = center.y + radius * Math.sin(angle * Math.PI / 180);
	      return {
	          'x': x,
	          'y': y
	      };
    };

    var placement = function(key) {
	      var value;
	      value = values.get(key);
	      if (!values.has(key)) {
	          value = place(key);
	      }
	      return value;
    };
    var place = function(key) {
	      var value;
	      value = radialLocation(center, current, radius);
	      values.set(key, value);
	      current += increment;
	      return value;
    };
    var setKeys = function(keys) {
	      var firstCircleCount, firstCircleKeys, secondCircleKeys;
	      values = d3.map();
	      firstCircleCount = 360 / increment + 1;
	      if (keys.length < firstCircleCount) {
	          increment = 360 / keys.length;
	      }
	      firstCircleKeys = keys.slice(0, firstCircleCount);
	      firstCircleKeys.forEach(
	          function(k) {
		            return place(k);
	          });
	      secondCircleKeys = keys.slice(firstCircleCount);
	      radius = radius - (radius / 1.8);
	      increment = 360 / secondCircleKeys.length;
	      return secondCircleKeys.forEach(
	          function(k) {
		            return place(k);
	          });
    };

    placement.keys = function(_) {
	      if (!arguments.length) {
	          return d3.keys(values);
	      }
	      setKeys(_);
	      return placement;
    };
    placement.center = function(_) {
	      if (!arguments.length) {
	          return center;
	      }
	      center = _;
	      return placement;
    };
    placement.radius = function(_) {
	      if (!arguments.length) {
	          return radius;
	      }
	      radius = _;
	      return placement;
    };
    placement.start = function(_) {
	      if (!arguments.length) {
	          return start;
	      }
	      start = _;
	      current = start;
	      return placement;
    };
    placement.increment = function(_) {
	      if (!arguments.length) {
	          return increment;
	      }
	      increment = _;
	      return placement;
    };
    return placement;
};

/**
 * Object handling d3js force object
 */
Network = function() {
    var vis;

    width = jQuery(window).width();//1920;
    height = jQuery(window).height()-21;

    var min_radius = 5;
    var max_radius = 15;

    var dist_force = 15;
    var dist_radial = 15;

    // initialise data container
    var allData = [];
    var curLinksData = [];
    var curNodesData = [];
    var linkedByIndex = {};
    var nodesG = null;
    var linksG = null;
    var node = null;
    var link = null;

    var refs = [];
    var lst_dates = [];
    var lst_anim = [];

    // set current parameters
    var layout = 'force';
    var sort = 'songs';
    var linksFilter = 'aucun';
    var groupCenters = null;

    var force = d3.layout.force();

    var tooltip = Tooltip('vis-tooltip', 230);

    var nodeColors = function(i) {
	      return col[i];
    };
    var charge = function(node) {
	      return -Math.pow(node.radius, 2.0) / 1.5;
    };

    /** Contructor */
    var network = function(selection, data) {
	      var start_zoom = 1;
	      var start_center_x = -(width/2)*(start_zoom-1);
	      var start_center_y = -(height/2)*(start_zoom-1);
	      var zoom = function() {
	          vis.attr('transform', 'translate(' + d3.event.translate + ')scale(' + d3.event.scale + ')');
	      };

	      allData = setupData(data);
	      vis = d3.select(selection)
	          .append('svg')
	          .attr('width', width)
	          .attr('height', height)
	      //.attr('transform', 'translate('+start_center_x+',' + start_center_y +')scale(' + start_zoom  + ')')
	          .call(d3.behavior.zoom().scaleExtent([0.1, 8]).on('zoom', zoom))
	          .append('g');
	      linksG = vis.append('g').attr('id', 'links');
	      nodesG = vis.append('g').attr('id', 'nodes');
	      force.size([width, height]);
	      setLayout(layout);
	      legend();
	      return update();
    };

    var setupData = function(data) {
	      var nodesMap;

	      refs = data.refs;
	      data.nodes.forEach(
	          function(n) {
		            if (n.group == 0){
		                lst_dates.push(n.date);
		            }
		            var randomnumber;
		            n.x = randomnumber = Math.floor(Math.random() * width);
		            n.y = randomnumber = Math.floor(Math.random() * height);
		            return n.radius = map_range(n.size, 1, 30, min_radius, max_radius);
	          });
	      nodesMap = mapNodes(data.nodes);
	      data.links.forEach(
	          function(l) {
		            l.source = nodesMap.get(l.source);
		            l.target = nodesMap.get(l.target);
		            return linkedByIndex['' + l.source.id + ',' + l.target.id] = 1;
	          });

	      return data;
    };

    var update = function() {
	      var artists;
	      //var links_min = 2;
	      curNodesData = filterNodes(allData.nodes);
	      curNodesData = filterRange(curNodesData, em_min, em_max);
	      //curNodesData = filterNodesByLinks(curNodesData, links_min);
	      curLinksData = filterLinks(allData.links, curNodesData);
	      if (layout === 'radial') {
	          node = force.nodes();
	          node.forEach(
		            function(d){
		                d.fixed = false;
		            }); 
	          artists = sortArtists(curNodesData, curLinksData);
	          network.artists = artists;
	          updateCenters(artists);
	      }else{
	          node = force.nodes();
	          node.forEach(
		            function(d){
		                d.fixed = false;
		            });
	      }
	      force.nodes(curNodesData);
	      curNodesData.forEach(
	          function(n) {
		            var r = map_range(n.size, 1, 30, min_radius, max_radius);
		            if (r>30){
		                return n.radius = max_radius;
		            }else{
		                return n.radius = r;
		            }
	          });
	      updateNodes();
	      if (layout === 'force') {
	          force.links(curLinksData);
	          updateLinks();
	      } else {
	          force.links(curLinksData);
	          updateLinks();
	          force.on('tick', radialTick).charge(-500).linkDistance(dist_radial).linkStrength(0);
	      }

	      network.nodes = curNodesData;
	      network.links = curLinksData;
	      return force.start();
    };
    var updateCenters = function(artists) {
	      if (layout === 'radial') {
	          return groupCenters = RadialPlacement()
		            .center({
			              'x': width / 2,
			              'y': height / 2.})
		            .radius((em_max-em_min)*4) // define radial radius
		            .increment(1)
		            .keys(artists);
	      }else{
	          return 0;
	      }
    };
    var updateNodes = function() {
	      node = nodesG.selectAll('circle.node')
	          .data(curNodesData,
		              function(d) {
		                  return d.id;
		              });
	      node.attr('r',
		              function(d) {
		                  return d.radius;
		              });
	      node.enter().append('circle')

	          .attr('class', 'node')
	          .attr('cx',
		              function(d) {
		                  return d.x;
		              })
	          .attr('cy',
		              function(d) {
		                  return d.y;
		              })
	          .style('fill',
		               function(d) {
		                   return nodeColors(d.group);
		               })
	          .style('stroke',
		               function(d) {
		                   return strokeFor(d);
		               })
	          .style('stroke-width', 1.0)
	          .call(force.drag)
	          .transition()
	          .duration(500)
	          .attr('r',
		              function(d) {
		                  return d.radius;
		              });

	      node.on('mouseover', showDetails).on('mouseout', hideDetails);
	      return node.exit()
	          .transition()
	          .duration(500)
	          .attr('r', 0)
	          .remove();
    };
    var updateLinks = function() {
	      link = linksG.selectAll('line.link')
	          .data(curLinksData,
		              function(d) {
		                  return '' + d.source.id + '_' + d.target.id;
		              });

	      link.enter().append('line')
	          .attr('class', 'link')
	          .attr('stroke', '#ddd')
	          .attr('stroke-opacity', 0.0)
	          .style('stroke-width',
		               function(d) {
	    	               if ((d.source.group + d.target.group) == 0){
	    		                 return 7;
	    	               }else{
	    		                 return 1;
	    	               }
		               })
	          .attr('x1', function(d) {return d.source.x;})
	          .attr('y1', function(d) {return d.source.y;})
	          .attr('x2', function(d) {return d.target.x;})
	          .attr('y2', function(d) {return d.target.y;})
	          .transition()
	          .duration(500)
	          .attr('stroke-opacity', function(d){return 0.8;});
	      return link.exit()
	          .transition()
	          .duration(500)
	          .attr('stroke-opacity', function(d){return 0;})
	          .remove();
    };

    var filterNodes = function(allNodes) {
	      var cutoff, filteredNodes, playcounts;
	      filteredNodes = allNodes.filter(
	          function(n) {
		            return lstFilter[n.group]['active'];
	          });
	      return filteredNodes;
    };
    var filterNodesByLinks = function(nodes, links_m){
	      return nodes.filter(
	          function(n){
		            return n.size > links_m;
	          });
    };
    var filterLinks = function(allLinks, curNodes) {
	      var filteredLinks;
	      curNodes = mapNodes(curNodes);

	      filteredLinks = allLinks.filter(
	          function(l) {
		            return curNodes.get(l.source.id) && curNodes.get(l.target.id);
	          });

	      if (linksFilter === 'aucun'){
	          filteredLinks = filteredLinks.filter(
		            function(l){
		                return (l.source.group + l.target.group) != 0;
		            });
	      }
	      return filteredLinks;
    };
    var filterRange = function(allNodes, em_min, em_max){
	      var emNodes, autNodes, filteredNodes, con_size, texte_dates, texte_numeros;

	      texte_dates = _('du ') + lst_dates[em_min] + _(' au ') + lst_dates[em_max-1];
	      texte_numeros = _('numéros ') + (em_min+1) + _(' à ') + (em_max);
	      d3.select('#range_info_dates').text(texte_dates);
	      d3.select('#range_info_numeros').text(texte_numeros);
	      filteredNodes = allNodes.filter(
	          function (n){

	          });
	      emNodes = allNodes.filter(
	          function(n){
		            return n.num > em_min && n.num <= em_max;
	          });
	      autNodes = allNodes.filter(
	          function(n){
		            return n.group !=0;
	          });
	      autNodes.forEach(
	          function (n){
		            con_size = getEmConnected(n, emNodes);
		            if (con_size>0){
		                n.size = con_size;
	 	                filteredNodes.push(n);
		            }
	          });
	      emNodes.forEach(
	          function (n){
		            filteredNodes.push(n);
	          });
	      return filteredNodes;
    };

    var mapNodes = function(nodes) {
	      var nodesMap;
	      nodesMap = d3.map();
	      nodes.forEach(
	          function(n) {
		            return nodesMap.set(n.id, n);
	          });
	      return nodesMap;
    };
    var neighboring = function(a, b) {
	      return linkedByIndex[a.id + ',' + b.id] || linkedByIndex[b.id + ',' + a.id];
    };
    var getEmConnected = function(aut_node, emNodes){
	      var con_size;
	      con_size = 0;
	      emNodes.forEach(
	          function (n){
		            if (neighboring(n, aut_node)){
		                con_size += 1;
		            }
	          });
	      return con_size;
    };

    var sortArtists = function(nodes, links) {
	      return lst_dates.slice(em_min, em_max);
    };

    var setLayout = function(newLayout) {
	      layout = newLayout;
	      if (layout === 'force') {
	          return force.on('tick', forceTick)
		            .charge(-200)
		            .linkDistance(dist_force)
		            .linkStrength(
		                function(l){
			                  var ww = 1;
			                  if (l.source.group != 0){
			                      ww = l.source.weight;
			                  }else if (l.target.group != 0){
			                      ww = l.target.weight;
			                  }
			                  if (ww == 0){ww = 1;}
			                  return 2/(ww);
		                });
	      }
        //else if (layout === 'radial') {
	      return force.on('tick', radialTick).charge(-500).linkDistance(dist_radial).linkStrength(1);
        //}
    };
    var setLinksFilter = function(newLinksFilter) {
	      return linksFilter = newLinksFilter;
    };
    var setSort = function(newSort) {
	      return sort = newSort;
    };

    var forceTick = function(e) {
	      node.attr('cx', function(d) {return d.x;})
	          .attr('cy', function(d) {return d.y;});
	      return link
	          .attr('x1', function(d) {return d.source.x;})
	          .attr('y1', function(d) {return d.source.y;})
	          .attr('x2', function(d) {return d.target.x;})
	          .attr('y2', function(d) {return d.target.y;});
    };

    var radialTick = function(e) {
	      node.each(moveToRadialLayout(e.alpha));
	      node.attr('cx', function(d) {return d.x;})
	          .attr('cy', function(d) {return d.y;});

	      link.attr('x1', function(d) {return d.source.x;})
	          .attr('y1', function(d) {return d.source.y;})
	          .attr('x2', function(d) {return d.target.x;})
	          .attr('y2', function(d) {return d.target.y;});

	      if (e.alpha < 0.093) {
	          force.stop();
	          node.each(
		            function(d){
		                if (d.group == 0){
			                  d.fixed = true;
		                }
		            });
	          force.on('tick', forceTick)
		            .charge(-500/3)
		            .linkDistance(map_range(em_max - em_min, 0, 90,20,20))
		            .linkStrength(1);
	          force.start();
	          return ;
	      }
    };

    var moveToRadialLayout = function(alpha) {
	      var k = 0.8;//(alpha + 0.5)* 0.5;
	      return function(d) {
	          if (d.group ==0){
		            var centerNode;
		            centerNode = groupCenters(d.date);
		            d.x += (centerNode.x - d.x) * k;
		            return d.y += (centerNode.y - d.y) * k;
	          }else{
		            //var centerNode;
		            //centerNode = groupCenters(d.date);
		            d.x = d.px;
		            return d.y = d.py;
	          }
	      };
    };

    // tooltips // infos
    var strokeFor = function(d) {
	      return d3.rgb(nodeColors(d.group)).darker().toString();
    };
    var showDetails = function(d, i) {
	      var lien_wiki = ' ';
	      var content = '<p class="main">' + d.name + '</span></p>';
	      content += '<hr class="tooltip-hr">';
	      if (d.type === 'emission'){
	          content += '<p class="main"> n°'+(d.id + 1).toString()+', date : ' + d.date + '</span></p>';
	      }else if (d.type === 'article'){
	          content += '<p class="main">' +d.annee +' '+  d.auteur + '</span></p>';
	      }else{
	          if (d.deces === ' '){
		            content += '<p class="main">' + d.naissance + '</span>'+lien_wiki+'</p>';
	          }else if (d.naissance!="?"){
		            content += '<p class="main">' + d.naissance + ' - ' + d.deces + '</span>'+lien_wiki+'</p>';
	          }
	          //content += '<p class="main">' + d.size + '</span></p>';
	      }
	      tooltip.showTooltip(content, d3.event);
	      if (link) {
	          link.attr('stroke', function(l) {
			          if (l.source === d || l.target === d) {
			              return '#fff';
			          } else {
			              return '#555';
			          }
		        })
		            .attr('stroke-opacity', function(l) {
			              if (l.source === d || l.target === d) {
			                  return 1.0;
			              } else {
			                  return 0.8;
			              }
		            });
	      }
	      node.style('stroke', function(n) {
		        if (n.searched || neighboring(d, n)) {
			          return '#fff';
		        } else {
			          return strokeFor(n);
		        }
		    })
	          .style('stroke-width', function(n) {
		            if (n.searched) {
			              return 10.0;
		            } else if (neighboring(d, n)){
			              return 2.0;
		            } else {
				            return 1.0;
		            }
		        });
	      makeInfos(d);

	      return d3.select(this).style('stroke', 'white').style('stroke-width', 2.0);
    };
    var makeInfos = function(d) {
	      var content, lst_ref, cpt;
	      lst_ref = [];
	      if (d.type == 'emission'){
	          content = makeInfosEmission(d);
	      }else if(d.type == 'auteur'){
	          content = makeInfosAuteur(d);
	      }else if(d.type == 'article'){
	          content = makeInfosArticle(d);
	      }
	      jQuery('#infos').html(content);
	      jQuery('#infos').show();

    };
    var makeInfosEmission = function(d){
	      var content, lst_aut, dic_ref;
	      lst_aut = [];
	      dic_ref = d3.map();
	      content = '<h2>' + _("Infos Émission") +'</h2>';
	      //content += '<h3> Titre : </h3>';
	      content += '<h3> ' + d.name + '</h3>';
	      content += '<p>' + _('numéro :') + ' ' + (d.id + 1) + '</p>';
	      content += '<p>' + _('date de diffusion :') + ' ' + d.date + '</p>';
	      content += '<a href="'+d.lien+'" target=_blank>' + _('lien France Inter') + '</a>';
	      // recupere la liste des references citees par auteur connecte
	      node.each(
	          function(n){
		            if (neighboring(d,n) && n.type == 'auteur'){
		                lst_aut.push(n.name);
		                dic_ref[n.name] = n.ids_ref;
		            }
	          });
	      // ajoute le contenu auteurs + chaque reference citee
	      if (lst_aut.length>0){
	          content += "<h3>" + _('Auteurs cités')+"</h3>";
	          lst_aut.sort();
	          lst_aut.forEach(
		            function(a){
		                content += '<p class="aut"> ' + a + "</p>";
		                dic_ref[a].forEach(
			                  function(r){
			                      if (d.ids_ref.indexOf(r)>=0){
				                        content += '<div class="ref_emission">' + appendRef(r, '') + '</div>';
			                      }
			                  });
		            });
	      }
	      return content;
    };
    var makeInfosArticle = function(d){
	      var content, lst_em;
	      lst_em = [];
	      content = '<h2>' + _("Infos Article") +'</h2>';
	      //content += "<h3> Nom : </h3>";
	      content += "<h3> " + d.name + "</h3>";
	      content += "<p>" + _('Publication :') + ' '+ d.annee + "</p>";
	      content += "<p>" + _('Auteurs :') + ' '+ d.auteur + "</p>";
	      content += "<p>" + _('Revue :') + ' '+ d.revue + "</p>";

	      node.each(function(n){
		        if (neighboring(d,n) && n.type == 'emission'){
			          lst_em.push(n.name);
		        }
		    });

	      if (lst_em.length>1){
	          content += "<h3>" + _('Émissions') +"</h3>";
	      }else{
	          content += "<h3>" + _('Émission') +"</h3>";
	      }

	      lst_em.forEach(function(e){
			      content += '<p class="em"> ' + e + "</p>";
		    });

	      return content;
    };
    var makeInfosAuteur = function(d){
	      var content, lst_em, lst_ref;
	      lst_em = [];
	      lst_ref = [];
	      content = "<h2>" + _('Infos Auteur') + "</h2>";
	      //content += "<h3> Nom : </h3>";
	      content += "<h3> " + d.name + "</h3>";
	      if (d.deces === ' '){
	          content += '<p>' + _('naissance :') + ' ' + d.naissance + '</p>';
	      }else if (d.naissance != '?') {
	          content += '<p>' + _('naissance :') + ' ' + d.naissance + ' - ' + _('décès :') + ' ' + d.deces + '</p>';
	      }
	      if (d.wiki){
	          content += '<a href="'+d.wiki+'" target=_blank>'+_('lien Wikipedia') +'</a>';
	      }

	      node.each(
	          function(n){
		            if (neighboring(d,n)){
		                //content += '<p class="em"> ' + n.name + "</p>";
		                var em = d3.map();
		                em['em'] = n;
		                em['lst_ref'] = [];
		                lst_em.push(em);
		                n.ids_ref.forEach(
			                  function(r){
			                      if (d.ids_ref.indexOf(r)>=0){
				                        if(lst_ref.indexOf(r) < 0){
				                            em['lst_ref'].push(lst_ref.length);
				                            lst_ref.push(r);
				                            //content += appendRef(r);
				                        }else{
				                            em['lst_ref'].push(lst_ref.indexOf(r));
				                        }
			                      }
			                  });
		            }
	          });

	      if (lst_ref.length>1){
	          content += "<h3>" + _('Références') + "</h3>";
	          var cpt = 1;
	          lst_ref.forEach(
		            function(r){
		                var plus = '<span class="ref_id"> [' + cpt + ']</span>';
		                cpt += 1;
		                content += '<div class="ref_auteur">' + appendRef(r, plus) + '</div>';
		            });
	      }else{
	          content += "<h3>" + _('Référence') + "</h3>";
	          lst_ref.forEach(
		            function(r){
		                content += '<div class="ref_auteur">' + appendRef(r, '') + '</div>';
		            });
	      }

	      if (lst_em.length>1){
	          content += "<h3>" + _('Émissions') +"</h3>";
	      }else{
	          content += "<h3>" + _('Émission') +"</h3>";
	      }

	      if (lst_ref.length>1){
	          lst_em.forEach(
		            function(e){
		                content += '<p class="em"> ' + e.em.name + "    [";
		                e.lst_ref.forEach(function(r){
					              content += (r+1) + ', ';
				            });
		                content = content.slice(0,content.length-2) +'] <p>';
		            });
	      }else{
	          lst_em.forEach(
		            function(e){
		                content += '<p class="em"> ' + e.em.name + "</p>";
		            });
	      }
	      return content;
    };
    var appendRef = function(r, plus) {
	      var content;
	      content='';

	      content += '<div class="ref">' +plus+ '<span class="titre_ref">'
	          + refs[r]['titre']
	          + '</span> <br> <span class="infos_ref">';
	      if (refs[r]['editeur'] != ' '){
	          content += refs[r]['editeur']
		            + ', ';
	      }
	      content += refs[r]['parution'] + "</span></div>";
	      return content;
    };
    var hideDetails = function(d, i) {
	      tooltip.hideTooltip();
	      node.style("stroke", function(n) {
		        if (!n.searched) {
			          return strokeFor(n);
		        } else {
			          return "#fff";
		        }
		    })
	          .style("stroke-width", function(n) {
		            if (!n.searched) {
			              return 1.0;
		            } else {
			              return 10.0;
		            }
		        });
	      if (link) {
	          return link.attr("stroke", "#ddd").attr("stroke-opacity", 0.8);
	      }
	      return 0;
    };

    // interaction
    network.toggleLayout = function(newLayout) {
	      force.stop();
	      setLayout(newLayout);
	      return update();
    };
    network.toggleFilter = function() {
	      force.stop();
	      return update();
    };
    network.toggleLinksFilter = function(newLinksFilter) {
	      force.stop();

	      setLinksFilter(newLinksFilter);
	      return update();
    };
    network.toggleSort = function(newSort) {
	      force.stop();
	      setSort(newSort);
	      return update();
    };
    network.changeRange = function(em_min_in, em_max_in){
        if (em_min_in == em_min && em_max_in == em_max){
            return;
        }
	      force.stop();

	      em_min = em_min_in;
	      em_max = em_max_in;

	      return update();
    };
    network.tweenRange = function(){
	      var t, time_range;
	      if (lst_anim.length>0){
	          network.stopTween();
	          network.changeRange(bak_em_min, bak_em_max);
	      }
	      var bak_em_min = em_min;
	      var bak_em_max = em_max;
	      time_range = d3.range(0, (em_max-em_min));
	      time_range.forEach(
	          function(t){
		            var anim;
		            anim = setTimeout(
		                function(){
			                  network.changeRange(em_min, em_min+t);
		                }, (t)*550);
		            lst_anim.push(anim);
	          });
    };
    network.stopTween = function(){
	      if (lst_anim.length>0){
	          lst_anim.forEach(function(d){
				        clearTimeout(d);
			      });
	          lst_anim = [];
	      }
    };
    network.updateSearch = function(searchTerm) {
	      var searchRegEx;
	      searchRegEx = new RegExp(searchTerm.toLowerCase());
	      return node.each(function(d) {
		        var element, match;
		        element = d3.select(this);
		        match = d.name.toLowerCase().search(searchRegEx);
		        if (searchTerm.length > 0 && match >= 0) {
		            element
			              .style("fill", "#FFF7")
			              .style("stroke-width", 10.0)
			              .style("stroke", "#fff");
		            return d.searched = true;
		        } else {
		            d.searched = false;
		            return element.style(
                    "fill",
					          function(d) {
					              return nodeColors(d.group);
					          })
                    .style("stroke-width", 1.0);
		        }
	      });
    };
    network.updateData = function(newData) {
	      allData = setupData(newData);
	      link.remove();
	      node.remove();

	      return update();
    };

    return network;
};


/**
 * Controls
 */

var activate = function(group, link) {
    d3.selectAll("#" + group + " a").classed("active", false);
    return d3.selectAll("#" + group + " #" + link).classed("active", true);
};
var check = function(group, link) {
    if (dicFilter[link]['active']){
	      dicFilter[link]['active'] = false;
    }else{
	      dicFilter[link]['active'] = true;
    }
    return d3.selectAll("#" + group + " #" + link).classed("active", dicFilter[link]['active']);
};

jQuery(function() {
	  myNetwork = Network();

	  d3.selectAll(".animer")
        .on("click", function(){
					  return myNetwork.tweenRange();
				});
	  d3.selectAll("#disposition a")
        .on("click", function(d) {
				    var newLayout = d3.select(this).attr("id");
				    activate("disposition", newLayout);
				    return myNetwork.toggleLayout(newLayout);
		    });
	  d3.selectAll("#filtres a")
        .on("click", function(d) {
				    var newFilter = d3.select(this).attr("id");
				    check("filtres", newFilter);
				    return myNetwork.toggleFilter();
		    });
	  d3.selectAll("#liens a")
        .on("click", function(d) {
				    var newLinksFilter = d3.select(this).attr("id");
				    activate("liens", newLinksFilter);
				    return myNetwork.toggleLinksFilter(newLinksFilter);
		    });
	  d3.selectAll("#classement a")
        .on("click", function(d) {
				    var newSort = d3.select(this).attr("id");
				    activate("classement", newSort);
				    return myNetwork.toggleSort(newSort);
		    });
	  jQuery("#search").keyup(function() {
				var searchTerm = jQuery(this).val();
				return myNetwork.updateSearch(searchTerm);
		});
    var slider = new rSlider({
        target: '#rslider',
        values: {min: 0, max: em_n},
        range: true,
        step: 1,
        tooltip: false,
        scale: false,
        labels: false,
        set: [em_min, em_max]
    });
    jQuery("#slider-range").on('mousemove', ()=>{
        var values = slider.getValue().split(',');
        var em_min = values[0];
        var em_max = values[1];
        myNetwork.stopTween();
        myNetwork.changeRange(em_min, em_max);
    });
	  return d3.json("./data/sledd.json", function(json) {
			  return myNetwork("#vis", json);
		});
});

jQuery(window).resize(function(){
    width = jQuery(window).width() ;//1920;
    height = jQuery(window).height();
    d3.select("#vis")
	      .selectAll("svg")
	      .attr("width", width)
	      .attr("height", height);
});

/**
 * Control panel
 */

jQuery(
    function($){
	      var show_apropos = false;
	      var show_modedemploi = false;
	      var apropos = $("#apropos"); 
	      var modedemploi = $("#modedemploi");

	      var control = $("#controls");
	      var a_apropos = $("#a_apropos");
	      var a_modedemploi = $("#a_modedemploi");

	      control.mouseover(
	          function(){
		            if ($(window).width() < 1150){
		                control.animate({left:0}, 100);
		            }
	          });

	      control.mouseleave(
	          function(){
		            if ($(window).width() < 1150){
		                //control.attr("style","left:-40%;")
		                control.animate({left:"-45%"}, 100);
		            }
	          });

	      var fit_apropos = function(){
	          if ($(window).width() < 1150){
		            a_apropos.after(apropos);
		            a_modedemploi.after(modedemploi);
	          }else{
		            control.after(apropos);
		            control.after(modedemploi);
	          }
	      };

	      fit_apropos();
	      $(window).resize(
	          function(){
		            fit_apropos();
		            if ($(window).width() > 1149){
		                control.animate({left:0});
		            }
	          });

	      a_apropos.click(
	          function(){
		            if (show_apropos == false){
		                modedemploi.attr("style","display:none;");
		                apropos.attr("style", "display:block;");
		                show_apropos = true;
		                show_modedemploi = false;
		            }else{
		                apropos.attr("style", "display:none;");
		                show_apropos = false;
		            }
	          });
	      a_modedemploi.click(
	          function(){
		            if (show_modedemploi == false){
		                apropos.attr("style", "display:none;");
		                modedemploi.attr("style", "display:block;");
		                show_apropos = false;
		                show_modedemploi = true;
		            }else{
		                modedemploi.attr("style", "display:none;");
		                show_modedemploi = false;
		            }
	          });
    });
